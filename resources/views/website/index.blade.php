@extends('layouts.app')
@section('content')
<div class="full-size d-flex align-items-center justify-content-center">
	<div class="container">
		<div class="row">
			<div class="col-sm-5 mx-auto">
				<img src="{{ asset('img/svg/LOGO_OUKISHI.svg') }}" class="img-fluid" alt="">
			</div>
		</div>
	</div>
</div>
@stop